function [ theta ] = getTrunkLeanRightAngle( w, x, y, z, cameraTiltAngle )
%TRUNKLEANRIGHTANGLE in degrees, positive if lean right. In world
%coordinate.
% w, x, y, z is the quaternion data at spineMid or spineShoulder  

t = cameraTiltAngle;
R = [   1   0       0       ;
        0   cos(t)  -sin(t) ;
        0   sin(t)  cos(t)  ];

vector = R * quaternionToBoneOrientation(w, x, y, z)';
theta = atan2(vector(1), vector(2));  % in c#, use Math.Atan2
theta = 180/pi * theta;


end

