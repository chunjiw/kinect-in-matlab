function [ border ] = getBorderTargets( visitedMap )
%GETBORDERTARGETS Returns the border targets, based on visited targets.
%   definition: one target is on border if and only if 
%   1. it's previously not visited;
%   2. it has neighbor targets visited. (diagonal targets are not neighbors.)
%   input: visitedMap is a binary map of size 3*5

border = zeros(3,5);

for i = 1:3
    for j = 1:5
        if visitedMap(i,j)
            continue;
        end
        if i-1 > 0 && visitedMap(i-1, j)
            border(i,j) = 1;
            continue;
        end
        if i+1 < 4 && visitedMap(i+1, j)
            border(i,j) = 1;
            continue;
        end
        if j-1 > 0 && visitedMap(i, j-1)
            border(i,j) = 1;
            continue;
        end
        if j+1 < 6 && visitedMap(i, j+1)
            border(i,j) = 1;
            continue;
        end    
    end
end

