function [ rotated_vector ] = quaternionToBoneNormal( w, x, y, z )
%QUATERNIONTOBONENORMAL converts quaternion to 3d vector representing bone
%normal vector
% The bone normal is perpendicular to the parent bone. 
% e.g. upper arm bone normal is pointing out from elbow, and the
% quaternion data comes from elbow.

% bone normal is aligned to z direction
% https://social.msdn.microsoft.com/Forums/en-US/31c9aff6-7dab-433d-9af9-59942dfd3d69/kinect-v20-preview-sdk-jointorientation-vs-boneorientation?forum=kinectv2sdk
rx = 0;
ry = 0;
rz = 1;

% rotate [rx, ry, rz] to [vx, vy, vz]
vx = (1 - 2 * (y * y + z * z)) * rx + 2 * (x * y - z * w) * ry + 2 * (z * x + y * w) * rz;
vy = (1 - 2 * (x * x + z * z)) * ry + 2 * (y * z - x * w) * rz + 2 * (x * y + z * w) * rx;
vz = (1 - 2 * (x * x + y * y)) * rz + 2 * (z * x - y * w) * rx + 2 * (y * z + x * w) * ry;

rotated_vector = [vx, vy, vz];
            
end

