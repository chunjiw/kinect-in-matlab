function [ flexion ] = getShoulderFlexionAngle( spineShoulderOri, upperArmOri, sagittalNormal )
%GETSHOULDERFLEXIONANGLE Gives shoulder flexion angle
% For definition go to http://azopt.net/wp-content/uploads/2013/01/MD0556_img_28.jpg
% Mathematically, it's defined as the angle between spineShoulder and
% upperArm in sagittal plane.

spineShoulderOriInSagittalPlane = spineShoulderOri - (sagittalNormal * spineShoulderOri') * sagittalNormal;
upperArmOriInSagittalPlane = upperArmOri - (upperArmOri*sagittalNormal') * sagittalNormal;
flexion = cosAngle(upperArmOriInSagittalPlane, -spineShoulderOriInSagittalPlane);

end

