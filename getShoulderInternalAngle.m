function [ internal ] = getShoulderInternalAngle( upperArmOriNormal )
%GETSHOULDERINTERNALANGLE Gets the shoulder internal angle
% For definition go to http://azopt.net/wp-content/uploads/2013/01/MD0556_img_28.jpg
% corresponds to "inward/outward rotation"

internal = cosAngle(upperArmOriNormal, [0, 0, 1]);

end

