%% connect and get cursor
clear
conn = database('KinectSource','sa','P@ssw0rd');
results = runsqlscript(conn,'sessionData.sql');
data = results.Data;

%% time
N = size(data,1);
time = datetime([],[],[]);
for i = 1:N
    time(i) = datetime(data{i,21},'InputFormat', 'yyyy-MM-dd HH:mm:ss.SSSSSSS');
end
time = time';
%% get data
% spineMid
spineMidPosX = [data{:, 142}];
spineMidPosY = [data{:, 143}];
spineMidPosZ = [data{:, 144}];
spineMidOriX = [data{:, 145}];
spineMidOriY = [data{:, 146}];
spineMidOriZ = [data{:, 147}];
spineMidOriW = [data{:, 148}];
% spineShoulder should be preferred when it comes to trunk orientation
spineShoulderPosX = [data{:, 38}];
spineShoulderPosY = [data{:, 39}];
spineShoulderPosZ = [data{:, 40}];
spineShoulderOriX = [data{:, 41}];
spineShoulderOriY = [data{:, 42}];
spineShoulderOriZ = [data{:, 43}];
spineShoulderOriW = [data{:, 44}];
% shoulder
shoulderLeftPosX = [data{:, 46}];
shoulderLeftPosY = [data{:, 47}];
shoulderLeftPosZ = [data{:, 48}];
shoulderRightPosX = [data{:, 54}];
shoulderRightPosY = [data{:, 55}];
shoulderRightPosZ = [data{:, 56}];
% elbow 
elbowPosX = [data{:,70}]';
elbowPosY = [data{:,71}]';
elbowPosZ = [data{:,72}]';
elbowOriX = [data{:,73}]';
elbowOriY = [data{:,74}]';
elbowOriZ = [data{:,75}]';
elbowOriW = [data{:,76}]';
% head
headOriX = [data{:,25}]';
headOriY = [data{:,26}]';
headOriZ = [data{:,27}]';
headOriW = [data{:,28}]';

%% plot
% shoulder angles
coronalNormal = nan(N,3);
sagittalNormal = nan(N,3);
spineShoulderOri = nan(N,3);
upperArmOri = nan(N,3);
shoulderFlexion = nan(N,1);
shoulderAbduction = nan(N,1);
for i = 1:N
    spineShoulderOri(i,:) = quaternionToBoneOrientation(spineShoulderOriW(i), spineShoulderOriX(i), spineShoulderOriY(i), spineShoulderOriZ(i));
    upperArmOri(i,:) = quaternionToBoneOrientation(elbowOriW(i), elbowOriX(i), elbowOriY(i), elbowOriZ(i));
    coronalNormal(i,:) = getCoronalPlaneNormal(... %%[shoulderLeftPosX(i), shoulderLeftPosY(i), shoulderLeftPosZ(i)],...            
        [spineShoulderPosX(i), spineShoulderPosY(i), spineShoulderPosZ(i)],...
        [shoulderRightPosX(i), shoulderRightPosY(i), shoulderRightPosZ(i)],...
        [spineMidPosX(i), spineMidPosY(i), spineMidPosZ(i)]);
    sagittalNormal(i,:) = getSagittalPlaneNormal(coronalNormal(i,:), spineShoulderOri(i,:));
    shoulderAbduction(i) = getShoulderAbductionAngle(spineShoulderOri(i,:), upperArmOri(i,:), coronalNormal(i,:));
    shoulderFlexion(i) = getShoulderFlexionAngle(spineShoulderOri(i,:), upperArmOri(i,:), sagittalNormal(i,:));
end
% plot(time, shoulderAbduction, '.-', time, shoulderFlexion, '.-r');
% head angles
headTurn = nan(N,1);
headTilt = nan(N,1);
headNod = nan(N,1);
headOri = nan(N,3);
headOriNormal = nan(N,3);
for i = 1:N
    headOri(i,:) = quaternionToBoneOrientation(headOriW(i), headOriX(i), headOriY(i), headOriZ(i));
    headOriNormal(i,:) = quaternionToBoneNormal(headOriW(i), headOriX(i), headOriY(i), headOriZ(i));
    headTurn(i) = getHeadTurnAngle(-headOriNormal(i,:), coronalNormal(i,:));
    headNod(i) = getHeadNodAngle(spineShoulderOri(i,:), headOri(i,:), sagittalNormal(i,:));
    headTilt(i) = getHeadTiltAngle(spineShoulderOri(i,:), headOri(i,:), coronalNormal(i,:));
end
plot(time, headTurn, '.-', time, headNod, '.-', time, headTilt, '.-');
legend('headTurn', 'headNod', 'headTilt')


