function [ turn ] = getTrunkTurnAngle( coronalNormal, cameraTiltAngle )
%GETTRUNKTURNANGLE Gives the turn angle of trunk, in world coordinate.
%   
t = cameraTiltAngle;
R = [   1   0       0       ;
        0   cos(t)  -sin(t) ;
        0   sin(t)  cos(t)  ];
coronalNormal = R * coronalNormal';
coronalNormal(2) = 0;   % ignore up direction. This angle is calculated in horizontal plane.
turn = cosAngle(coronalNormal, [0 0 -1]);

end

