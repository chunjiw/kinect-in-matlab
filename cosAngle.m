function [ theta ] = cosAngle( a, b )
%COSANGLE Summary of this function goes here
%   Detailed explanation goes here

a = a/norm(a);
b = b'/norm(b);
theta = acosd(a(1)*b(1) + a(2)*b(2) + a(3)*b(3));

end