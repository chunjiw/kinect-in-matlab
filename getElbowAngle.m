function [ elbowAngle ] = getElbowAngle( upperArmOri, lowerArmOri )
%GETELBOWANGLE Gives the elbow flexion angle
%   Detailed explanation goes here

elbowAngle = cosAngle(upperArmOri, lowerArmOri);

end

