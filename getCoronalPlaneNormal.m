function [ coronalNormal ] = getCoronalPlaneNormal( shoulderLeftPos, shoulderRightPos, spineMidPos )
%CORONALPLANENORMAL Gives the normal vector of coronal plane (in camera space). coronal plane
%is defined by three points: left and right shoulder, and spineMid.

left = shoulderLeftPos - spineMidPos;
right = shoulderRightPos - spineMidPos;
% cross product, need to implement in C#.
% cross product: https://en.wikipedia.org/wiki/Cross_product
coronalNormal = cross(left, right);     
% normalize
coronalNormal = coronalNormal/norm(coronalNormal);

end

