function [ tilt ] = getHeadTiltAngle( spineShoulderOri, headOri, coronalNormal )
%GETHEADTILTANGLE Gives head nod angle
% Mathematically, it's defined as the angle between spineShoulder and
% head in coronal plane.
% left and right tilts both return positive values.

spineShoulderOriInCoronalPlane = spineShoulderOri - (coronalNormal * spineShoulderOri') * coronalNormal;
headOriInCoronalPlane = headOri - (headOri*coronalNormal') * coronalNormal;
tilt = cosAngle(headOriInCoronalPlane, spineShoulderOriInCoronalPlane);

end

