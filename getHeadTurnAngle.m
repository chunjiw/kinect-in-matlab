function [ turn ] = getHeadTurnAngle( headOriNormal, coronalNormal )
%GETHEADTURNANGLE gets the turn angle of head
%   It's defined as the angle between normal vector of head and normal
%   vector of coronal plane. It's zero if looking forward.
%   important: head orientation quaternion is not available from Kinect, so
%   there wouldn't be headOriNormal. use neckOriNormal instead. But the
%   result is much less sensitive to real head turn.

turn = cosAngle(headOriNormal, coronalNormal);


end

