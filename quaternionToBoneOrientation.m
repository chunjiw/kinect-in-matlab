function [ rotated_vector ] = quaternionToBoneOrientation( w, x, y, z )
%QUATERNIONTOBONEORIENTATION converts quaternion to 3d vector representing bone
%orientation
% The bone direction is from parent joint to child joint (current joint), 
% e.g. upper arm direction is pointing from shoulder to elbow, and the
% quaternion data comes from elbow.

% bone direction is aligned to y direction
% https://social.msdn.microsoft.com/Forums/en-US/31c9aff6-7dab-433d-9af9-59942dfd3d69/kinect-v20-preview-sdk-jointorientation-vs-boneorientation?forum=kinectv2sdk
rx = 0;
ry = 1;
rz = 0;

% rotate [rx, ry, rz] to [vx, vy, vz]
vx = (1 - 2 * (y * y + z * z)) * rx + 2 * (x * y - z * w) * ry + 2 * (z * x + y * w) * rz;
vy = (1 - 2 * (x * x + z * z)) * ry + 2 * (y * z - x * w) * rz + 2 * (x * y + z * w) * rx;
vz = (1 - 2 * (x * x + y * y)) * rz + 2 * (z * x - y * w) * rx + 2 * (y * z + x * w) * ry;

rotated_vector = [vx, vy, vz];
            
end

