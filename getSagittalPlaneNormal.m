function [ sagittalNormal ] = getSagittalPlaneNormal( coronalNormal, spineShoulderOri )
%SAGITTALPLANENORMAL Gives normal vector of the sagittal plane.
%   
sagittalNormal = cross(coronalNormal, spineShoulderOri);
sagittalNormal = sagittalNormal/norm(sagittalNormal);

end

