function [ height ] = getElbowHeight( eX, eY, eZ, cameraTiltAngle )
%ELBOWHEIGHT returns elbow height in world coordinates.
%   Directly read location data from Kinect and rotated to compensate
%   camera tilt.
% height is the vertical distance from elbow to shoulder, so being negative
% means elbow is below the shoulder.

% sX is shoulderPositionX
% eX is elbowPositionX
% rotation matrix is a theta-rotation along x-axis.
t = cameraTiltAngle;
R = [   1   0       0       ;
        0   cos(t)  -sin(t) ;
        0   sin(t)  cos(t)  ];
elbowInCameraSpace = [eX, eY, eZ]';
elbowInWorldSpace = R * elbowInCameraSpace;   % in C#, only the second component is needed.
height = elbowInWorldSpace(2);

end

