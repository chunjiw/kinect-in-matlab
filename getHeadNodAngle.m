function [ nod ] = getHeadNodAngle( spineShoulderOri, headOri, sagittalNormal )
%GETHEADNODANGLE Gives head nod angle
% Mathematically, it's defined as the angle between spineShoulder and
% head in sagittal plane.

spineShoulderOriInSagittalPlane = spineShoulderOri - (sagittalNormal * spineShoulderOri') * sagittalNormal;
headOriInSagittalPlane = headOri - (headOri*sagittalNormal') * sagittalNormal;
nod = cosAngle(headOriInSagittalPlane, spineShoulderOriInSagittalPlane);

end

