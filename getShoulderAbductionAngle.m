function [ abduction ] = getShoulderAbductionAngle( spineShoulderOri, upperArmOri, coronalNormal )
%GETSHOULDERABDUCTIONANGLE Gives shoulder abduction angle
% For definition go to http://azopt.net/wp-content/uploads/2013/01/MD0556_img_28.jpg
% Mathematically, it's defined as the angle between spineShoulder and
% upperArm in coronal plane.

% spineShoulderOriDotCoronalNormal = coronalNormal * spineShoulderOri';  %% this inner product is near zero (up to 0.05)
spineShoulderOriInCoronalPlane = spineShoulderOri - (coronalNormal * spineShoulderOri') * coronalNormal;
upperArmOriInCoronalPlane = upperArmOri - (upperArmOri*coronalNormal') * coronalNormal;
abduction = cosAngle(upperArmOriInCoronalPlane, -spineShoulderOriInCoronalPlane);

end

